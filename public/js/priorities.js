window.calculate_priority = () => {    
    let score = (ip) => {
        return parseInt(ip);
    };

    let default_ip = '127.0.0.1';
    let get_ip = () => {
        $.ajax({
            method: 'GET',
            url: "https://api.ipify.org?format=json",
            success: (data) => {
                console.log(data.ip);
                // use data.ip
            },
            error: () => {
                // use the default_ip
            },
        });
    };


    // TODO: why does this code not work?
    // we want it to get the ip address from the API, then return the
    // score through a promise
    return new Promise((resolve, reject) => {
        let ip = get_ip();
        resolve(score(ip));
    });

};